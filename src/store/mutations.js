export const SET_VISIBILITY_LAYER_LIST = (state) => {
  state.layerList.isVisible = !state.layerList.isVisible;
};
