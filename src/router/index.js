import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/pages/Home'
import About from '@/components/pages/About'
import Skills from '@/components/pages/Skills'
import Works from '@/components/pages/Works'
import Contact from '@/components/pages/Contact'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    }, {
      path: '/About',
      name: 'About',
      component: About
    }, {
      path: '/Skills',
      name: 'Skills',
      component: Skills
    }, {
      path: '/Works',
      name: 'Works',
      component: Works
    }, {
      path: '/Contact',
      name: 'Contact',
      component: Contact
    }
  ]
})
